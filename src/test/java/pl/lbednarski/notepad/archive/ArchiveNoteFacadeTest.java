package pl.lbednarski.notepad.archive;

import pl.lbednarski.notepad.archive.dto.ArchiveNoteCmd;
import pl.lbednarski.notepad.infrastructure.NoteError;
import io.vavr.control.Either;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class ArchiveNoteFacadeTest {

    private ArchiveNoteFacade archiveNoteFacade;
    private final ArchiveNoteRepository repository = new ArchiveNoteRepositoryList();
    private final UUID noteId = UUID.fromString("225e13d8-9ac9-4eb9-946f-ca05c0dc2879");

    @BeforeEach
    void setUp() {
        archiveNoteFacade = new ArchiveNoteFacadeImpl(new ArchiveNoteValidator(), new ArchiveNoteService(repository));
    }

    @Test
    void shouldArchiveNotAndReturnIt() {
        var archivedNote = archiveNoteFacade.archive(new ArchiveNoteCmd(noteId, "Title", "Content", LocalDateTime.now())).get();

        Assertions.assertThat(archivedNote.getId()).isEqualTo(noteId);
        Assertions.assertThat(archivedNote.getTitle()).isEqualTo("Title");
        Assertions.assertThat(archivedNote.getContent()).isEqualTo("Content");
        Assertions.assertThat(archivedNote.getCreatedAt()).isNotNull();
        Assertions.assertThat(archivedNote.getArchivedAt()).isNotNull();
    }

    @Test
    void shouldRemoveExistingNote() {
        repository.save(new ArchiveNoteEntity(noteId, "Title", "Content", LocalDateTime.now(), LocalDateTime.now()));
        archiveNoteFacade.removeById(noteId.toString());

        Assertions.assertThat(archiveNoteFacade.getById(noteId.toString()))
                .isEqualTo(Either.left(NoteError.NOTE_DOES_NOT_EXIST_ERROR));
    }
}