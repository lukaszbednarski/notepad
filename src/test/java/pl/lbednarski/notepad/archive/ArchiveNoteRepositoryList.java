package pl.lbednarski.notepad.archive;

import io.vavr.collection.Stream;
import io.vavr.control.Option;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

class ArchiveNoteRepositoryList implements ArchiveNoteRepository {

    private final Set<ArchiveNoteEntity> notes = Collections.synchronizedSet(new HashSet<>());

    @Override
    public ArchiveNoteEntity save(ArchiveNoteEntity newEntity) {
        delete(newEntity);
        notes.add(newEntity);

        return newEntity;
    }

    @Override
    public Option<ArchiveNoteEntity> getById(UUID id) {
        return Stream.ofAll(notes).find(entity -> entity.getId().equals(id));
    }

    @Override
    public void delete(ArchiveNoteEntity newEntity) {
        notes.removeIf(entity -> entity.getId().equals(newEntity.getId()));
    }
}
