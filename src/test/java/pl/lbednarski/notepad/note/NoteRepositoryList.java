package pl.lbednarski.notepad.note;

import io.vavr.collection.Stream;
import io.vavr.control.Option;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

class NoteRepositoryList implements NoteRepository {

    private final Set<NoteEntity> notes = Collections.synchronizedSet(new HashSet<>());

    @Override
    public NoteEntity save(NoteEntity newEntity) {
        delete(newEntity);
        notes.add(newEntity);

        return newEntity;
    }

    @Override
    public Option<NoteEntity> getById(UUID id) {
        return Stream.ofAll(notes).find(entity -> entity.getId().equals(id));
    }

    @Override
    public void delete(NoteEntity newEntity) {
        notes.removeIf(entity -> entity.getId().equals(newEntity.getId()));
    }

}
