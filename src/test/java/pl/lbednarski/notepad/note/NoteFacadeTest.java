package pl.lbednarski.notepad.note;

import pl.lbednarski.notepad.archive.ArchiveNoteFacade;
import pl.lbednarski.notepad.archive.dto.ArchiveNoteCmd;
import pl.lbednarski.notepad.archive.dto.ArchiveNoteDto;
import pl.lbednarski.notepad.infrastructure.NoteError;
import pl.lbednarski.notepad.note.dto.NoteCmd;
import pl.lbednarski.notepad.note.dto.UpdateNoteCmd;
import io.vavr.control.Either;
import io.vavr.control.Option;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class NoteFacadeTest {

    @Mock
    private IdGenerator idGenerator;
    @Mock
    private ArchiveNoteFacade archiveNoteFacade;

    private NoteFacade noteFacade;
    private final NoteRepository noteRepository = new NoteRepositoryList();
    private final UUID noteId = UUID.fromString("225e13d8-9ac9-4eb9-946f-ca05c0dc2879");

    @BeforeEach
    void setUp() {
        noteFacade = new NoteFacadeImpl(new NoteService(noteRepository, idGenerator), new NoteValidator(), archiveNoteFacade);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "   "})
    void shouldNotSaveInvalidNote(String invalidTitle) {
        var result = noteFacade.save(new NoteCmd(invalidTitle, "Content"));

        assertThat(result).isEqualTo(Either.left(NoteError.VALIDATION_TITLE_NOTE_ERROR));
    }

    @Test
    void shouldReturnSavedNoteAfterSaveNote() {
        when(idGenerator.generateId()).thenReturn(noteId);

        var savedNote = noteFacade.save(new NoteCmd("Title", "Content")).get();

        assertThat(savedNote.getId()).isEqualTo(noteId);
        assertThat(savedNote.getTitle()).isEqualTo("Title");
        assertThat(savedNote.getContent()).isEqualTo("Content");
        assertThat(savedNote.getCreatedAt()).isNotNull();
        assertThat(savedNote.getModifiedAt()).isNotNull();
    }

    @Test
    void shouldRemoveExistingNote() {
        noteRepository.save(new NoteEntity(noteId, "Title", "Content", LocalDateTime.now(), LocalDateTime.now()));

        var result = noteFacade.removeById(noteId.toString());

        assertThat(result).isEqualTo(Either.right(noteId));
        assertThat(noteRepository.getById(noteId)).isEqualTo(Option.none());
    }

    @Test
    void shouldUpdateExistingNote() {
        noteRepository.save(new NoteEntity(noteId, "Title", "Content", LocalDateTime.now(), LocalDateTime.now()));
        var updatedNote = noteFacade.update(new UpdateNoteCmd(noteId.toString(), "New Title", "New Content")).get();

        assertThat(updatedNote.getId()).isEqualTo(noteId);
        assertThat(updatedNote.getTitle()).isEqualTo("New Title");
        assertThat(updatedNote.getContent()).isEqualTo("New Content");
        assertThat(updatedNote.getCreatedAt()).isNotNull();
        assertThat(updatedNote.getModifiedAt()).isNotNull();
    }

    @Test
    void shouldRemoveNoteAfterArchive() {
        when(archiveNoteFacade.archive(any(ArchiveNoteCmd.class)))
                .thenReturn(Either.right(new ArchiveNoteDto(noteId, "Title", "Content", LocalDateTime.now(), LocalDateTime.now())));
        noteRepository.save(new NoteEntity(noteId, "Title", "Content", LocalDateTime.now(), LocalDateTime.now()));

        noteFacade.archive(noteId.toString());

        assertThat(noteFacade.getById(noteId.toString()))
                .isEqualTo(Either.left(NoteError.NOTE_DOES_NOT_EXIST_ERROR));
    }

    @Test
    void shouldCallArchiveNoteFacade() {
        when(archiveNoteFacade.archive(any(ArchiveNoteCmd.class)))
                .thenReturn(Either.right(new ArchiveNoteDto(noteId, "Title", "Content", LocalDateTime.now(), LocalDateTime.now())));
        noteRepository.save(new NoteEntity(noteId, "Title", "Content", LocalDateTime.now(), LocalDateTime.now()));

        noteFacade.archive(noteId.toString());

        verify(archiveNoteFacade, times(1)).archive(any(ArchiveNoteCmd.class));
    }
}