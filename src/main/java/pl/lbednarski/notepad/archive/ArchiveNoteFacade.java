package pl.lbednarski.notepad.archive;

import pl.lbednarski.notepad.archive.dto.ArchiveNoteCmd;
import pl.lbednarski.notepad.archive.dto.ArchiveNoteDto;
import pl.lbednarski.notepad.infrastructure.NoteError;
import io.vavr.control.Either;

import java.util.UUID;

public interface ArchiveNoteFacade {

    Either<NoteError, ArchiveNoteDto> archive(ArchiveNoteCmd note);

    Either<NoteError, ArchiveNoteDto> getById(String id);

    Either<NoteError, UUID> removeById(String id);

}
