package pl.lbednarski.notepad.archive;

import pl.lbednarski.notepad.archive.dto.ArchiveNoteCmd;
import pl.lbednarski.notepad.archive.dto.ArchiveNoteDto;
import pl.lbednarski.notepad.infrastructure.NoteError;
import io.vavr.control.Either;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.UUID;

@Slf4j
@Component
class ArchiveNoteFacadeImpl implements ArchiveNoteFacade {

    private final ArchiveNoteValidator noteValidator;
    private final ArchiveNoteService archiveNoteService;

    ArchiveNoteFacadeImpl(ArchiveNoteValidator noteValidator, ArchiveNoteService archiveNoteService) {
        this.noteValidator = Objects.requireNonNull(noteValidator, "ArchiveNoteValidator must be defined.");
        this.archiveNoteService = Objects.requireNonNull(archiveNoteService, "ArchiveNoteService must be defined.");
    }

    @Override
    public Either<NoteError, ArchiveNoteDto> archive(ArchiveNoteCmd note) {
        return noteValidator.validateArchiveNoteCmd(note)
                .map(archiveNoteService::save)
                .map(this::convertToDto);
    }

    @Override
    public Either<NoteError, ArchiveNoteDto> getById(String id) {
        return noteValidator.validateId(id)
                .flatMap(archiveNoteService::getById)
                .peekLeft(error -> log.warn("Could not fetch note. Reason: {}", error.getMessage()))
                .map(this::convertToDto);
    }

    @Override
    public Either<NoteError, UUID> removeById(String id) {
        return noteValidator.validateId(id)
                .flatMap(archiveNoteService::removeById)
                .peekLeft(error -> log.warn("Could not remove new note. Reason: {}", error.getMessage()));
    }

    private ArchiveNoteDto convertToDto(ArchiveNoteEntity entity) {
        return ArchiveNoteDto.builder()
                .id(entity.getId())
                .title(entity.getTitle())
                .content(entity.getContent())
                .createdAt(entity.getCreatedAt())
                .archivedAt(entity.getArchivedAt())
                .build();
    }
}
