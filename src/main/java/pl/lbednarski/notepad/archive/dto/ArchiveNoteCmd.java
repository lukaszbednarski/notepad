package pl.lbednarski.notepad.archive.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
public class ArchiveNoteCmd {

    private UUID id;
    private String title;
    private String content;
    private LocalDateTime createdAt;

}
