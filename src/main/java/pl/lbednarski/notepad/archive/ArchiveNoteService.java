package pl.lbednarski.notepad.archive;

import pl.lbednarski.notepad.archive.dto.ArchiveNoteCmd;
import pl.lbednarski.notepad.infrastructure.NoteError;
import io.vavr.control.Either;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@Service
class ArchiveNoteService {

    private final ArchiveNoteRepository archiveNoteRepository;

    ArchiveNoteService(ArchiveNoteRepository archiveNoteRepository) {
        this.archiveNoteRepository = Objects.requireNonNull(archiveNoteRepository, "ArchiveNoteRepository must be defined.");
    }

    ArchiveNoteEntity save(ArchiveNoteCmd note) {
        ArchiveNoteEntity newNote = ArchiveNoteEntity.builder()
                .id(note.getId())
                .title(note.getTitle())
                .content(note.getContent())
                .createdAt(note.getCreatedAt())
                .archivedAt(LocalDateTime.now())
                .build();

        return archiveNoteRepository.save(newNote);
    }

    Either<NoteError, ArchiveNoteEntity> getById(UUID id) {
        return archiveNoteRepository.getById(id)
                .toEither(NoteError.NOTE_DOES_NOT_EXIST_ERROR);
    }

    Either<NoteError, UUID> removeById(UUID id) {
        return getById(id)
                .peek(archiveNoteRepository::delete)
                .map(ArchiveNoteEntity::getId);
    }
}
