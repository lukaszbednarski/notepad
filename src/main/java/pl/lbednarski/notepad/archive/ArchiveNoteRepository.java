package pl.lbednarski.notepad.archive;

import io.vavr.control.Option;
import org.springframework.data.repository.Repository;

import java.util.UUID;

interface ArchiveNoteRepository extends Repository<ArchiveNoteEntity, UUID> {

    ArchiveNoteEntity save(ArchiveNoteEntity entity);

    Option<ArchiveNoteEntity> getById(UUID id);

    void delete(ArchiveNoteEntity entity);

}


