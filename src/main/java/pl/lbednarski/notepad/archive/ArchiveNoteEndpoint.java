package pl.lbednarski.notepad.archive;

import pl.lbednarski.notepad.infrastructure.ResponseResolver;
import io.vavr.Function1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
class ArchiveNoteEndpoint {

    private final ArchiveNoteFacade archiveNoteFacade;

    @Autowired
    ArchiveNoteEndpoint(ArchiveNoteFacade archiveNoteFacade) {
        this.archiveNoteFacade = Objects.requireNonNull(archiveNoteFacade, "ArchiveNoteFacade must be defined.");
    }

    @GetMapping("/notes/archive/{id}")
    ResponseEntity<?> getById(@PathVariable String id) {
        return Function1.of(ResponseResolver::resolve)
                .apply(archiveNoteFacade.getById(id));
    }

    @DeleteMapping("/notes/archive/{id}")
    ResponseEntity<?> removeById(@PathVariable String id) {
        return Function1.of(ResponseResolver::resolve)
                .apply(archiveNoteFacade.removeById(id));
    }
}
