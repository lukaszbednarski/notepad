package pl.lbednarski.notepad.archive;

import pl.lbednarski.notepad.archive.dto.ArchiveNoteCmd;
import pl.lbednarski.notepad.infrastructure.NoteError;
import io.vavr.collection.Seq;
import io.vavr.control.Either;
import io.vavr.control.Validation;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

import static io.vavr.API.*;

@Component
class ArchiveNoteValidator {

    private final String uuidPattern = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";

    Either<NoteError, ArchiveNoteCmd> validateArchiveNoteCmd(ArchiveNoteCmd noteCmd) {
        return Validation.combine(
                idExist(noteCmd.getId()),
                validateTitle(noteCmd.getTitle()),
                validateContent(noteCmd.getContent()),
                validateCreateDate(noteCmd.getCreatedAt())
        ).ap(ArchiveNoteCmd::new).mapError(Seq::head).toEither();
    }

    private Validation<NoteError, String> validateTitle(String title) {
        return title.isBlank()
                ? Invalid(NoteError.VALIDATION_TITLE_NOTE_ERROR)
                : Valid(title);
    }

    private Validation<NoteError, String> validateContent(String title) {
        return title.isBlank()
                ? Invalid(NoteError.VALIDATION_CONTENT_NOTE_ERROR)
                : Valid(title);
    }

    private Validation<NoteError, LocalDateTime> validateCreateDate(LocalDateTime createdAt) {
        return Objects.nonNull(createdAt) && createdAt.isBefore(LocalDateTime.now())
                ? Valid(createdAt)
                : Invalid(NoteError.VALIDATION_TITLE_NOTE_ERROR);
    }

    private Validation<NoteError, UUID> idExist(UUID id) {
        return Objects.nonNull(id)
                ? Valid(id)
                : Invalid(NoteError.VALIDATION_ID_NOTE_ERROR);
    }

    Either<NoteError, UUID> validateId(String noteId) {
        return Match(noteId).of(
                Case($(id -> Objects.nonNull(id) && id.matches(uuidPattern)), Either.right(UUID.fromString(noteId))),
                Case($(), Either.left(NoteError.VALIDATION_ID_NOTE_ERROR))
        );
    }
}
