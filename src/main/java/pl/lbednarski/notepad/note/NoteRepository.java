package pl.lbednarski.notepad.note;

import io.vavr.control.Option;
import org.springframework.data.repository.Repository;

import java.util.UUID;

interface NoteRepository extends Repository<NoteEntity, UUID> {

    NoteEntity save(NoteEntity entity);

    Option<NoteEntity> getById(UUID id);

    void delete(NoteEntity entity);

}


