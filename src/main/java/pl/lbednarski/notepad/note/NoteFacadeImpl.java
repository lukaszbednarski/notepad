package pl.lbednarski.notepad.note;

import org.springframework.transaction.annotation.Transactional;
import pl.lbednarski.notepad.archive.ArchiveNoteFacade;
import pl.lbednarski.notepad.archive.dto.ArchiveNoteCmd;
import pl.lbednarski.notepad.archive.dto.ArchiveNoteDto;
import pl.lbednarski.notepad.infrastructure.NoteError;
import pl.lbednarski.notepad.note.dto.NoteCmd;
import pl.lbednarski.notepad.note.dto.NoteDto;
import pl.lbednarski.notepad.note.dto.UpdateNoteCmd;
import io.vavr.control.Either;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.UUID;

@Slf4j
@Component
class NoteFacadeImpl implements NoteFacade {

    private final NoteService noteService;
    private final NoteValidator noteValidator;
    private final ArchiveNoteFacade archiveNoteFacade;

    NoteFacadeImpl(NoteService noteService, NoteValidator noteValidator, ArchiveNoteFacade archiveNoteFacade) {
        this.noteService = Objects.requireNonNull(noteService, "NoteService must be defined.");
        this.noteValidator = Objects.requireNonNull(noteValidator, "NoteValidator must be defined.");
        this.archiveNoteFacade = Objects.requireNonNull(archiveNoteFacade, "ArchiveNoteFacade must be defined.");
    }

    @Override
    public Either<NoteError, NoteDto> save(NoteCmd note) {
        return noteValidator.validateNoteCmd(note)
                .peekLeft(error -> log.warn("Could not save new note. Reason: {}", error.getMessage()))
                .map(noteService::save)
                .peek(entity -> log.info("New note has been created. Id: {}", entity.getId()))
                .map(this::convertToDto);
    }

    @Override
    public Either<NoteError, NoteDto> update(UpdateNoteCmd note) {
        return noteValidator.validateUpdateNoteCmd(note)
                .peekLeft(error -> log.warn("Could not update new note. Reason: {}", error.getMessage()))
                .flatMap(noteService::update)
                .map(this::convertToDto);
    }

    @Override
    public Either<NoteError, NoteDto> getById(String id) {
        return noteValidator.validateId(id)
                .peekLeft(error -> log.warn("Could not fetch note. Reason: {}", error.getMessage()))
                .flatMap(noteService::getById)
                .map(this::convertToDto);
    }

    @Override
    public Either<NoteError, UUID> removeById(String id) {
        return noteValidator.validateId(id)
                .peekLeft(error -> log.warn("Could not remove new note. Reason: {}", error.getMessage()))
                .flatMap(noteService::removeById);
    }

    @Override
    @Transactional
    public Either<NoteError, ArchiveNoteDto> archive(String id) {
        return getById(id)
                .map(note -> new ArchiveNoteCmd(note.getId(), note.getTitle(), note.getContent(), note.getCreatedAt()))
                .flatMap(archiveNoteFacade::archive)
                .peekLeft(error -> log.warn("Could not archive note. Reason: {}", error.getMessage()))
                .peek(note -> noteService.removeById(note.getId()));
    }

    private NoteDto convertToDto(NoteEntity entity) {
        return NoteDto.builder()
                .id(entity.getId())
                .title(entity.getTitle())
                .content(entity.getContent())
                .createdAt(entity.getCreatedAt())
                .modifiedAt(entity.getModifiedAt())
                .build();
    }

}
