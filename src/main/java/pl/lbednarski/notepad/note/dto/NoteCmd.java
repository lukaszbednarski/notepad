package pl.lbednarski.notepad.note.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NoteCmd {

    private String title;
    private String content;

}
