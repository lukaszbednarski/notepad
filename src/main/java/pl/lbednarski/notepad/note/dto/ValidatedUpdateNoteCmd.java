package pl.lbednarski.notepad.note.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class ValidatedUpdateNoteCmd {

    private UUID id;
    private String title;
    private String content;

}
