package pl.lbednarski.notepad.note;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
class IdGenerator {

    UUID generateId() {
        return UUID.randomUUID();
    }

}
