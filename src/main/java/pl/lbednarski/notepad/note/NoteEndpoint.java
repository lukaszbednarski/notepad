package pl.lbednarski.notepad.note;

import pl.lbednarski.notepad.infrastructure.ResponseResolver;
import pl.lbednarski.notepad.note.dto.NoteCmd;
import pl.lbednarski.notepad.note.dto.UpdateNoteCmd;
import io.vavr.Function1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
class NoteEndpoint {

    private final NoteFacade noteFacade;

    @Autowired
    NoteEndpoint(NoteFacade noteFacade) {
        this.noteFacade = Objects.requireNonNull(noteFacade, "NoteFacade must be defined.");
    }

    @PostMapping("/notes")
    ResponseEntity<?> save(@RequestBody NoteCmd noteCmd) {
        return Function1.of(ResponseResolver::resolve)
                .apply(noteFacade.save(noteCmd));
    }

    @GetMapping("/notes/{id}")
    ResponseEntity<?> getById(@PathVariable String id) {
        return Function1.of(ResponseResolver::resolve)
                .apply(noteFacade.getById(id));
    }

    @DeleteMapping("/notes/{id}")
    ResponseEntity<?> removeById(@PathVariable String id) {
        return Function1.of(ResponseResolver::resolve)
                .apply(noteFacade.removeById(id));
    }

    @PutMapping("/notes/{id}")
    ResponseEntity<?> update(@PathVariable String id, @RequestBody NoteCmd noteCmd) {
        return Function1.of(ResponseResolver::resolve)
                .apply(noteFacade.update(new UpdateNoteCmd(id, noteCmd.getTitle(), noteCmd.getContent())));
    }

    @PutMapping("/notes/{id}/archive")
    ResponseEntity<?> update(@PathVariable String id) {
        return Function1.of(ResponseResolver::resolve)
                .apply(noteFacade.archive(id));
    }
}
