package pl.lbednarski.notepad.note;

import pl.lbednarski.notepad.archive.dto.ArchiveNoteDto;
import pl.lbednarski.notepad.infrastructure.NoteError;
import pl.lbednarski.notepad.note.dto.NoteCmd;
import pl.lbednarski.notepad.note.dto.NoteDto;
import pl.lbednarski.notepad.note.dto.UpdateNoteCmd;
import io.vavr.control.Either;

import java.util.UUID;

public interface NoteFacade {

    Either<NoteError, NoteDto> save(NoteCmd note);

    Either<NoteError, NoteDto> update(UpdateNoteCmd note);

    Either<NoteError, NoteDto> getById(String id);

    Either<NoteError, UUID> removeById(String id);

    Either<NoteError, ArchiveNoteDto> archive(String id);

}
