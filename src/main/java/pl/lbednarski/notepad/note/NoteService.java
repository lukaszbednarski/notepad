package pl.lbednarski.notepad.note;

import org.springframework.transaction.annotation.Transactional;
import pl.lbednarski.notepad.infrastructure.NoteError;
import pl.lbednarski.notepad.note.dto.NoteCmd;
import pl.lbednarski.notepad.note.dto.ValidatedUpdateNoteCmd;
import io.vavr.control.Either;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@Service
class NoteService {

    private final NoteRepository noteRepository;
    private final IdGenerator idGenerator;

    NoteService(NoteRepository noteRepository, IdGenerator idGenerator) {
        this.noteRepository = Objects.requireNonNull(noteRepository, "NoteRepository must be defined.");
        this.idGenerator = Objects.requireNonNull(idGenerator, "IdGenerator must be defined.");
    }

    NoteEntity save(NoteCmd note) {
        var createdAt = LocalDateTime.now();
        NoteEntity newNote = NoteEntity.builder()
                .id(idGenerator.generateId())
                .title(note.getTitle())
                .content(note.getContent())
                .createdAt(createdAt)
                .modifiedAt(createdAt)
                .build();

        return noteRepository.save(newNote);
    }

    Either<NoteError, NoteEntity> getById(UUID id) {
        return noteRepository.getById(id)
                .toEither(NoteError.NOTE_DOES_NOT_EXIST_ERROR);
    }

    Either<NoteError, UUID> removeById(UUID id) {
        return getById(id)
                .peek(noteRepository::delete)
                .map(NoteEntity::getId);
    }

    @Transactional
    Either<NoteError, NoteEntity> update(ValidatedUpdateNoteCmd updateNoteCmd) {
        return getById(updateNoteCmd.getId())
                .map(entity -> updateEntity(entity, updateNoteCmd))
                .map(noteRepository::save);
    }

    private NoteEntity updateEntity(NoteEntity entity, ValidatedUpdateNoteCmd updateNoteCmd) {
        return NoteEntity.builder()
                .id(entity.getId())
                .title(updateNoteCmd.getTitle())
                .content(updateNoteCmd.getContent())
                .createdAt(entity.getCreatedAt())
                .modifiedAt(LocalDateTime.now())
                .build();
    }

}
