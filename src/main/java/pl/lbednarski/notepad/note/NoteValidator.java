package pl.lbednarski.notepad.note;

import pl.lbednarski.notepad.infrastructure.NoteError;
import pl.lbednarski.notepad.note.dto.NoteCmd;
import pl.lbednarski.notepad.note.dto.UpdateNoteCmd;
import pl.lbednarski.notepad.note.dto.ValidatedUpdateNoteCmd;
import io.vavr.collection.Seq;
import io.vavr.control.Either;
import io.vavr.control.Validation;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.UUID;

import static io.vavr.API.*;

@Component
class NoteValidator {

    private final String uuidPattern = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";

    Either<NoteError, NoteCmd> validateNoteCmd(NoteCmd noteCmd) {
        return Validation.combine(
                validateTitle(noteCmd.getTitle()),
                validateContent(noteCmd.getContent())
        ).ap(NoteCmd::new).mapError(Seq::head).toEither();
    }

    Either<NoteError, ValidatedUpdateNoteCmd> validateUpdateNoteCmd(UpdateNoteCmd noteCmd) {
        return Validation.combine(
                validateAndConvertId(noteCmd.getId()),
                validateTitle(noteCmd.getTitle()),
                validateContent(noteCmd.getContent())
        ).ap(ValidatedUpdateNoteCmd::new).mapError(Seq::head).toEither();
    }

    private Validation<NoteError, String> validateTitle(String title) {
        return title.isBlank()
                ? Invalid(NoteError.VALIDATION_TITLE_NOTE_ERROR)
                : Valid(title);
    }

    private Validation<NoteError, String> validateContent(String title) {
        return title.isBlank()
                ? Invalid(NoteError.VALIDATION_CONTENT_NOTE_ERROR)
                : Valid(title);
    }

    private Validation<NoteError, UUID> validateAndConvertId(String noteId) {
        return Objects.nonNull(noteId) && noteId.matches(uuidPattern)
                ? Valid(UUID.fromString(noteId))
                : Invalid(NoteError.VALIDATION_ID_NOTE_ERROR);
    }

    Either<NoteError, UUID> validateId(String noteId) {
        return Match(noteId).of(
                Case($(id -> Objects.nonNull(id) && id.matches(uuidPattern)), Either.right(UUID.fromString(noteId))),
                Case($(), Either.left(NoteError.VALIDATION_ID_NOTE_ERROR))
        );
    }
}
