package pl.lbednarski.notepad.infrastructure;

public enum NoteError implements ResponseError {

    VALIDATION_ID_NOTE_ERROR("Provided note id is invalid.", 400),
    VALIDATION_TITLE_NOTE_ERROR("Provided note title is invalid.", 400),
    VALIDATION_CONTENT_NOTE_ERROR("Provided note content is invalid.", 400),

    NOTE_DOES_NOT_EXIST_ERROR("Note with provided id doesn't exist or is already archived.", 404);

    private final int httpCode;
    private final String message;

    NoteError(String message, int httpCode) {
        this.message = message;
        this.httpCode = httpCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public int getHttpCode() {
        return httpCode;
    }
}