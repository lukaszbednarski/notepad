package pl.lbednarski.notepad.infrastructure;

import lombok.Data;

@Data
public class ErrorResponse {

    private final String message;

}