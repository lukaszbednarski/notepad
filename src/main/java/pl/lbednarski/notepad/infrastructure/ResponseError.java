package pl.lbednarski.notepad.infrastructure;

public interface ResponseError {

    String getMessage();

    int getHttpCode();

}