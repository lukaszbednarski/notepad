package pl.lbednarski.notepad.infrastructure;

import io.vavr.control.Either;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public final class ResponseResolver {

    private ResponseResolver() {
    }

    public static ResponseEntity<?> resolve(Either<? extends ResponseError, ?> input) {
        return input.map(ResponseEntity::ok)
                .getOrElseGet(ResponseResolver::createErrorResponse);
    }

    private static ResponseEntity createErrorResponse(ResponseError error) {
        int httpCode = error.getHttpCode();
        ErrorResponse response = new ErrorResponse(error.getMessage());

        return new ResponseEntity<>(response, HttpStatus.valueOf(httpCode));
    }
}