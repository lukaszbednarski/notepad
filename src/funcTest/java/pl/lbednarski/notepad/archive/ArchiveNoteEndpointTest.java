package pl.lbednarski.notepad.archive;

import io.vavr.control.Option;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ArchiveNoteEndpointTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ArchiveNoteRepository repository;

    private final UUID noteId = UUID.fromString("225e13d8-9ac9-4eb9-946f-ca05c0dc2879");

    @Test
    public void shouldGetNoteById() throws Exception {
        var currentDateTime = LocalDateTime.now();
        when(repository.getById(noteId))
                .thenReturn(Option.of(new ArchiveNoteEntity(noteId, "Title", "Content", currentDateTime, currentDateTime)));

        this.mockMvc.perform(get("/notes/archive/" + noteId.toString()))
                .andDo(print())
                .andExpect(status().isOk());

        verify(repository, times(1)).getById(noteId);
    }

    @Test
    public void shouldRemoveNoteById() throws Exception {
        var currentDateTime = LocalDateTime.now();
        var entity = new ArchiveNoteEntity(noteId, "Title", "Content", currentDateTime, currentDateTime);
        when(repository.getById(noteId)).thenReturn(Option.of(entity));

        this.mockMvc.perform(delete("/notes/archive/" + noteId.toString()))
                .andDo(print())
                .andExpect(status().isOk());

        verify(repository).getById(noteId);
        verify(repository).delete(entity);
    }
}