package pl.lbednarski.notepad.note;

import pl.lbednarski.notepad.archive.ArchiveNoteFacade;
import pl.lbednarski.notepad.archive.dto.ArchiveNoteCmd;
import pl.lbednarski.notepad.archive.dto.ArchiveNoteDto;
import io.vavr.control.Either;
import io.vavr.control.Option;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class NoteEndpointTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private NoteRepository repository;

    @MockBean
    private ArchiveNoteFacade archiveNoteFacade;

    private final UUID noteId = UUID.fromString("225e13d8-9ac9-4eb9-946f-ca05c0dc2879");

    @Test
    public void shouldSaveNote() throws Exception {
        var currentDateTime = LocalDateTime.now();
        when(repository.save(any(NoteEntity.class)))
                .thenReturn(new NoteEntity(noteId, "Title", "Content", currentDateTime, currentDateTime));

        this.mockMvc.perform(post("/notes")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"title\": \"title\",\n" +
                        "\t\"content\": \"blabla\"\n" +
                        "}"))
                .andDo(print())
                .andExpect(status().isOk());

        verify(repository).save(any(NoteEntity.class));
    }

    @Test
    public void shouldGetNoteById() throws Exception {
        var currentDateTime = LocalDateTime.now();
        when(repository.getById(noteId))
                .thenReturn(Option.of(new NoteEntity(noteId, "Title", "Content", currentDateTime, currentDateTime)));

        this.mockMvc.perform(get("/notes/" + noteId.toString()))
                .andDo(print())
                .andExpect(status().isOk());

        verify(repository, times(1)).getById(noteId);
    }

    @Test
    public void shouldRemoveNoteById() throws Exception {
        var currentDateTime = LocalDateTime.now();
        var entity = new NoteEntity(noteId, "Title", "Content", currentDateTime, currentDateTime);
        when(repository.getById(noteId)).thenReturn(Option.of(entity));

        this.mockMvc.perform(delete("/notes/" + noteId.toString()))
                .andDo(print())
                .andExpect(status().isOk());

        verify(repository).getById(noteId);
        verify(repository).delete(entity);
    }

    @Test
    public void shouldUpdateNote() throws Exception {
        var currentDateTime = LocalDateTime.now();
        var entity = new NoteEntity(noteId, "Title2", "Content2", currentDateTime, currentDateTime);
        when(repository.getById(noteId)).thenReturn(Option.of(entity));
        when(repository.save(any(NoteEntity.class))).thenReturn(entity);

        this.mockMvc.perform(put("/notes/" + noteId.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"title\": \"Title2\",\n" +
                        "\t\"content\": \"Content2\"\n" +
                        "}"))
                .andDo(print())
                .andExpect(status().isOk());

        verify(repository).getById(noteId);
        verify(repository).save(any(NoteEntity.class));
    }

    @Test
    public void shouldRemoveNoteAndSaveArchived() throws Exception {
        var currentDateTime = LocalDateTime.now();
        var entityNote = new NoteEntity(noteId, "Title", "Content", currentDateTime, currentDateTime);
        var archiveNoteCmd = new ArchiveNoteCmd(noteId, "Title", "Content", currentDateTime);
        var archiveNoteDto = new ArchiveNoteDto(noteId, "Title", "Content", currentDateTime, LocalDateTime.now());

        when(repository.getById(noteId)).thenReturn(Option.of(entityNote));
        when(archiveNoteFacade.archive(archiveNoteCmd)).thenReturn(Either.right(archiveNoteDto));

        this.mockMvc.perform(put("/notes/archive/" + noteId.toString()))
                .andDo(print())
                .andExpect(status().isOk());

        verify(repository, atLeastOnce()).getById(noteId);
        verify(repository).delete(entityNote);
        verify(archiveNoteFacade).archive(archiveNoteCmd);
    }
}